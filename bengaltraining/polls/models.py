from django.db import models
from django.contrib.auth.models import User
from profileinfo.models import interest

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField(auto_now_add=True)
    user=models.ForeignKey(User,default=None)
    tag=models.ForeignKey(interest,default=None,blank=False)
    total = models.IntegerField(default=0)
    music = models.IntegerField(default=0)
    painting = models.IntegerField(default=0)
    poetry = models.IntegerField(default=0)
    technology = models.IntegerField(default=0)
    vote = models.IntegerField(default=0)
    
    

    def __str__(self):
        return self.question_text
        

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __str__(self):
        return self.choice_text

class VoteViewCounter(models.Model):
    Question=models.OneToOneField(Question, on_delete=models.CASCADE)
    votedUser=models.ManyToManyField(User,related_name = 'votedUser')
    viewedUser=models.ManyToManyField(User, related_name = 'viewedUser')
    
    
    


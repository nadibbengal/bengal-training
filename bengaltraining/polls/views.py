from django.shortcuts import render,get_object_or_404,redirect
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse
from django.urls import reverse
from .models import Question,Choice,VoteViewCounter
from profileinfo.models import info
from django.contrib.auth.decorators import login_required
from . import forms
from django.core.paginator import Paginator

def index(request):
  
    latest_question_list = Question.objects.order_by('-pub_date')
    message = request.session.get('message')
    paginator=Paginator(latest_question_list,3)
    page=request.GET.get('page')
    if request.GET.get('page'):
        latest_question_list=paginator.page(page)
    else:
        latest_question_list=paginator.page(1)  
    context = {'latest_question_list': latest_question_list}
    if message!="no message":
        context = {'latest_question_list': latest_question_list,'message':message}
        request.session['message'] = "no message"
    return render(request, 'polls/index.html', context)


def profile(request):
    
    latest_question_list = Question.objects.order_by('-pub_date').filter(user=request.user)
    paginator=Paginator(latest_question_list,10)
    page=request.GET.get('page')
    if request.GET.get('page'):
        latest_question_list=paginator.page(page)
    else:
        latest_question_list=paginator.page(1)  
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/profile.html', context)


def admin(request):
    
    latest_question_list = Question.objects.order_by('-pub_date')
    paginator=Paginator(latest_question_list,10)
    page=request.GET.get('page')
    if request.GET.get('page'):
        latest_question_list=paginator.page(page)
    else:
        latest_question_list=paginator.page(1)  
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/admin.html', context)


@login_required(login_url="/accounts/signin/")    
def detail(request, question_id):
    
    question = get_object_or_404(Question, pk=question_id)
    user=question.user
    counter=VoteViewCounter.objects.get(Question=question)
    voters=counter.votedUser.all()
    viewers=counter.viewedUser.all()
    if request.user in voters:
        voterStatus="voter"
    else:
        voterStatus="non voter"
    if request.is_ajax():
        if request.user in viewers:
            #counting the interest clicks for each question
            print("here")
        else:
            profile=info.objects.all().filter(user=request.user)
            profile=profile[0]
            interests=profile.interest.all()
            if request.user!= question.user:
                question.total+=1
                # loop interests queryset to increment view counter
                for interest in interests:
                    selected=interest.title
                    print(selected)
                    if(selected=='music'):
                        question.music+=1
                    if(selected=='painting'):
                        question.painting+=1
                    if(selected=='peotry'):
                        question.poetry+=1
                    # common question save for every category
                question.save()
                counter.viewedUser.add(request.user)
                counter.save()
    
    
    return render(request, 'polls/details.html', {'question': question,'user':user,'voterstatus':voterStatus})

def getdata(request, question_id):
    
    question = get_object_or_404(Question, pk=question_id)
    choices=question.choice_set.all()
    listchoice=[]
    datas=[]
    # print(choices.count())
    for i in range(choices.count()):
        listchoice.append(choices[i].choice_text)
        datas.append(choices[i].votes)

    data={
        "choice":listchoice,
        "datas":datas
    }
    return JsonResponse(data)


#showing the result for super-user 
def results(request, question_id):
    
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/result.html', {'question': question})


def vote(request, question_id):
    
    question = get_object_or_404(Question, pk=question_id)
    question.vote+=1
    question.save()
    print(question.user)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/details.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        counter=VoteViewCounter.objects.get(Question=question)
        counter.votedUser.add(request.user)
        counter.save()
        latest_question_list = Question.objects.order_by('-pub_date')
        paginator=Paginator(latest_question_list,3)
        latest_question_list=paginator.page(1)
        message='Thank you for your vote. Please check out other questions'
        request.session['message'] = message
        return redirect("polls:index")


@login_required(login_url="/accounts/signin/")
def createpolls(request):
   
    if request.method=="POST":
        form=forms.createpoll(request.POST)
        choices=forms.takechoice(request.POST)
        #print(request.POST)
        try:
            if form.is_valid and choices.is_valid:
                temp=form.save(commit=False)
                temp.user=request.user
                temp.save()          
                qus=Question.objects.get(question_text=temp.question_text)
                counter=VoteViewCounter(Question=qus)
                counter.save()
                counter.votedUser.add(request.user)
                counter.save()
                counter.viewedUser.add(request.user)
                counter.save()
                x=request.POST.getlist('choice_text')
                #creating 3 choices
                for i in range(len(x)):
                    instance=Choice(question=qus,choice_text=x[i])
                    instance.save()
                #choices.save()
                message='Thank you for creating new question.Please check out other questions'
                request.session['message'] = message
                return redirect('polls:index')
        except:
            error="Your question was not valid!!"
            form=forms.createpoll()
            choices=forms.takechoice()
            return render(request,"polls/createpolls.html",{'form':form,'choice':choices,"error":error})
    else:
        form=forms.createpoll()
        choices=forms.takechoice()
    return render(request,"polls/createpolls.html",{'form':form,'choice':choices})